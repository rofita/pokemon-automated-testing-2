package com.blibli.future.rofita.steps;

import com.blibli.future.rofita.data.Pokemon;
import com.blibli.future.rofita.ui.pages.bulbapedia.BulbaHomePage;
import com.blibli.future.rofita.ui.pages.bulbapedia.BulbaPokemonDetailsPage;
import com.blibli.future.rofita.utils.PokemonUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.blibli.future.rofita.utils.PokemonUtil.*;
import static java.time.temporal.ChronoUnit.SECONDS;

public class GetBulbapediaDataSteps {
    BulbaHomePage bulbaHomePage;
    BulbaPokemonDetailsPage bulbaPokemonDetailsPage;
    SoftAssertions softAssert;

    @Given("prepare to get pokemon data from bulbapedia")
    public void prepareToGetPokemonDataFromBulbapedia() {
        softAssert = new SoftAssertions();
        bulbaHomePage.setImplicitTimeout( Integer.parseInt(getProperty("timeout.bulbapedia")), SECONDS);
        bulbaHomePage.getDriver().manage().window().maximize();
    }

    @Given("open bulbapedia homepage")
    public void openBulbapediaHomepage() {
        bulbaHomePage.open();
    }

    @When("get all pokemon data from bulbapedia")
    public void getAllPokemonDataFromBulbapedia() throws IOException {
        List<String> pokemonNameList = getPokemonNameList();

        for (String pokemonName : pokemonNameList) {
            Pokemon bulbaPokemon = new Pokemon();
            bulbaPokemon.pokemonTypeList = new ArrayList<>();
            bulbaPokemon.pokemonBaseStats = new HashMap<>();

            bulbaPokemon.pokemonName = pokemonName;
            bulbaHomePage.searchPokemonUsingSearchBar(pokemonName);

            softAssert.assertThat(bulbaPokemonDetailsPage.isTherePokemon(pokemonName))
                    .as(pokemonName + " data is not found").isTrue();

            if (bulbaPokemonDetailsPage.isTherePokemon(pokemonName)) {
                bulbaPokemon.pokemonNumber = bulbaPokemonDetailsPage.getPokemonNumber();
                bulbaPokemon.pokemonTypeList = bulbaPokemonDetailsPage.getPokemonTypes();
                bulbaPokemon.pokemonBaseStats = bulbaPokemonDetailsPage.getPokemonBaseStats();

                writePokemonToCSV(bulbaPokemon, getProperty("path.csv.bulbapedia"));
            }
            else {
                writePokemonNotFoundMessageToCSV( pokemonName, getProperty("path.csv.bulbapedia"));
            }
        }
    }

    @Then("get all pokemon data from bulbapedia should be success")
    public void getAllPokemonDataFromBulbapediaShouldBeSuccess() {
        softAssert.assertAll();
    }
}
