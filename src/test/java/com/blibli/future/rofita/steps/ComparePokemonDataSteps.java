package com.blibli.future.rofita.steps;

import com.blibli.future.rofita.data.Pokemon;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.assertj.core.api.SoftAssertions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;

import static com.blibli.future.rofita.utils.PokemonUtil.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ComparePokemonDataSteps {
    int totalOfPokemon;
    String pokemonName;
    SoftAssertions softAssert;

    @Given("prepare to compare {string} data from all sources")
    public void prepareToComparePokemonDataFromAllSources(String pokemonName) {
        softAssert = new SoftAssertions();
        totalOfPokemon = getPokemonNameList().size();
        this.pokemonName = pokemonName;
    }

    @When("compare pokemon data from {string} and {string}")
    public void comparePokemonDataFrom(String sourceA, String sourceB) throws IOException {
        Pokemon pokemonA = readPokemonFromCSV( pokemonName, getPathFromDataSource(sourceA));
        Pokemon pokemonB = readPokemonFromCSV( pokemonName, getPathFromDataSource(sourceB));

        softAssert.assertThat(pokemonA.pokemonNumber!=0 && pokemonB.pokemonNumber!=0).
                as("can't compare " + sourceA + " with " + sourceB + " because the pokemon data is not found")
                .isTrue();

        if (pokemonA.pokemonNumber!=0 && pokemonB.pokemonNumber!=0) {
            softAssert.assertThat(pokemonA.pokemonNumber).as("pokemon number isn't valid in comparison of " + sourceA + " and " + sourceB)
                    .isEqualTo(pokemonB.pokemonNumber);
            for (String baseStat : baseStatsKey) {
                softAssert.assertThat(pokemonA.pokemonBaseStats.get(baseStat)).as(baseStat + " isn't valid in comparison of " + sourceA + " and " + sourceB)
                        .isEqualTo(pokemonB.pokemonBaseStats.get(baseStat));
            }
            if (!(sourceA.equalsIgnoreCase("pokedex") ||
                    sourceB.equalsIgnoreCase("pokedex"))) {
                softAssert.assertThat(pokemonA.pokemonTypeList).as("pokemon types isn't valid in comparison of " + sourceA + " and " + sourceB)
                        .isEqualTo(pokemonB.pokemonTypeList);
            }
        }
    }

    @Then("pokemon data from all sources is valid")
    public void pokemonDataFromAllSourcesIsValid() {
        softAssert.assertAll();
    }

    @Given("check that pokemon data is ready to compare using {string} runner")
    public void checkThatPokemonDataIsReadyToCompareUsingPokemonRunner(String runner) throws IOException {
        assertThat (runner + " runner can't compare because data is not ready",
                isRunnerReadyDoCompare(runner), equalTo(true));
    }

    @And("ensure from {string} runner that pokemon data from other sources are ready")
    public void ensureFromPokeapiRunnerThatPokemonDataFromOtherSourcesAreReady(String runner) throws IOException {
        if (!isThisLastRunner(4))
            writeSignNotReadyToCSV(runner);
    }
}