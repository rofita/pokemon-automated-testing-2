package com.blibli.future.rofita.steps;

import com.blibli.future.rofita.data.Pokemon;
import com.blibli.future.rofita.ui.pages.pokedex.PokedexHomePage;
import com.blibli.future.rofita.ui.pages.pokedex.PokedexPokemonDetailsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.blibli.future.rofita.utils.PokemonUtil.*;
import static java.time.temporal.ChronoUnit.SECONDS;

public class GetPokedexDataSteps {
    PokedexHomePage pokedexHomePage;
    PokedexPokemonDetailsPage pokedexPokemonDetailsPage;
    SoftAssertions softAssert;

    @And("prepare to get pokemon data from pokedex")
    public void prepareToGetPokemonDataFromPokedex() {
        softAssert = new SoftAssertions();
        pokedexHomePage.setImplicitTimeout( Integer.parseInt(getProperty("timeout.pokedex")), SECONDS);
    }

    @And("get all pokemon data from pokedex")
    public void getAllPokemonDataFromPokedex() throws IOException {
        List<String> pokemonNameList = getPokemonNameList();
        pokedexHomePage.closePopup();

        for (String pokemonName : pokemonNameList) {
            Pokemon pokedexPokemon = new Pokemon();
            pokedexPokemon.pokemonTypeList = new ArrayList<>();
            pokedexPokemon.pokemonBaseStats = new HashMap<>();

            pokedexPokemon.pokemonName = pokemonName;
            pokedexHomePage.searchPokemonUsingSearchBar(pokemonName);

            softAssert.assertThat(pokedexHomePage.isTherePokemon(pokemonName))
                    .as(pokemonName + " data is not found").isTrue();

            if (pokedexHomePage.isTherePokemon(pokemonName)) {
                pokedexHomePage.openPokemonCard();
                pokedexPokemon.pokemonNumber = pokedexPokemonDetailsPage.getPokemonNumber();
                pokedexPokemon.pokemonBaseStats = pokedexPokemonDetailsPage.getPokemonBaseStats();

                pokedexPokemonDetailsPage.backToHomePage();

                writePokemonToCSV(pokedexPokemon, getProperty("path.csv.pokedex"));
            }
            else {
                writePokemonNotFoundMessageToCSV( pokemonName, getProperty("path.csv.pokedex"));
            }
        }
    }

    @Then("get all pokemon data from pokedex should be success")
    public void getAllPokemonDataFromPokedexShouldBeSuccess() {
        softAssert.assertAll();
    }
}
