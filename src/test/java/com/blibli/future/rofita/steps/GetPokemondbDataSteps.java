package com.blibli.future.rofita.steps;

import com.blibli.future.rofita.data.Pokemon;
import com.blibli.future.rofita.ui.pages.pokemondb.PokdbHomePage;
import com.blibli.future.rofita.ui.pages.pokemondb.PokdbPokemonListPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.blibli.future.rofita.utils.PokemonUtil.*;
import static com.blibli.future.rofita.utils.PokemonUtil.writePokemonToCSV;
import static java.time.temporal.ChronoUnit.SECONDS;

public class GetPokemondbDataSteps {
    PokdbHomePage pokdbHomePage;
    PokdbPokemonListPage pokdbPokemonListPage;
    SoftAssertions softAssert;

    @Given("prepare to get pokemon data from pokemondb")
    public void prepareToGetPokemonDataFromPokemondb() {
        softAssert = new SoftAssertions();
        pokdbHomePage.setImplicitTimeout( Integer.parseInt(getProperty("timeout.pokemondb")),SECONDS);
        pokdbHomePage.getDriver().manage().window().maximize();
    }

    @When("open pokemon list page in pokemondb")
    public void openPokemonListPageInPokemondb() throws InterruptedException {
        pokdbHomePage.open();
        pokdbHomePage.closePopup();
        pokdbHomePage.goToPokemonListPage();
    }

    @When("get all pokemon data from pokemondb")
    public void getAllPokemonDataFromPokemondb() throws IOException {
        List<String> pokemonNameList = getPokemonNameList();

        for (String pokemonName : pokemonNameList) {
            Pokemon pokedbPokemon = new Pokemon();

            pokedbPokemon.pokemonName = pokemonName;
            pokedbPokemon.pokemonTypeList = new ArrayList<>();
            pokedbPokemon.pokemonBaseStats = new HashMap<>();

            softAssert.assertThat(pokdbPokemonListPage.isTherePokemon(pokemonName))
                    .as(pokemonName + " data is not found").isTrue();

            if (pokdbPokemonListPage.isTherePokemon(pokemonName)) {
                pokdbPokemonListPage.setPokemonName(pokemonName);
                pokedbPokemon.pokemonNumber = pokdbPokemonListPage.getPokemonNumber();
                pokedbPokemon.pokemonTypeList = pokdbPokemonListPage.getPokemonTypes();
                pokedbPokemon.pokemonBaseStats = pokdbPokemonListPage.getPokemonBaseStats();

                writePokemonToCSV(pokedbPokemon, getProperty("path.csv.pokemondb"));
            }
            else {
                writePokemonNotFoundMessageToCSV( pokemonName, getProperty("path.csv.pokemondb"));
            }
        }
    }

    @Then("get all pokemon data from pokemondb should be success")
    public void getAllPokemonDataFromPokemondbShouldBeSuccess() {
        softAssert.assertAll();
    }
}
