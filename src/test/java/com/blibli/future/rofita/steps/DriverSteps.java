package com.blibli.future.rofita.steps;

import com.blibli.future.rofita.data.DriverData;
import io.cucumber.java.en.Given;

import static com.blibli.future.rofita.utils.PokemonUtil.getProperty;

public class DriverSteps {

    @Given("set up web driver")
    public void setUpWebDriver() {
        DriverData.setPlatformName(getProperty("browser.name"));
    }

    @Given("set up android driver")
    public void setUpAndroidDriver() {
        DriverData.setPlatformName("android");
    }
}
