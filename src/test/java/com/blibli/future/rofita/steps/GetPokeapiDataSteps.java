package com.blibli.future.rofita.steps;

import com.blibli.future.rofita.api.controllers.PokeapiController;
import com.blibli.future.rofita.api.data.responses.GetPokeDataResponse;
import com.blibli.future.rofita.data.Pokemon;
import com.blibli.future.rofita.utils.PokemonUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.blibli.future.rofita.utils.PokemonUtil.*;

public class GetPokeapiDataSteps {
    SoftAssertions softAssert;

    @Given("prepare to get pokemon data from pokeapi")
    public void prepareToGetPokemonDataFromPokeapi() {
        softAssert = new SoftAssertions();
    }

    @When("get all data from pokeapi")
    public void getAllDataFromPokeapi() throws IOException {
        List<String> pokemonNameList = getPokemonNameList();

        for (String pokemonName : pokemonNameList) {
            PokeapiController pokeapiController = new PokeapiController();
            Pokemon pokeapiPokemon = new Pokemon();

            pokeapiPokemon.pokemonName = pokemonName;
            pokeapiPokemon.pokemonTypeList = new ArrayList<>();
            pokeapiPokemon.pokemonBaseStats = new HashMap<>();

            Response response = pokeapiController.getPokemonData(pokeapiPokemon.pokemonName.toLowerCase());

            softAssert.assertThat(response.getStatusCode())
                    .as(pokemonName + " data is not found").isEqualTo(200);

            if (response.statusCode()==200) {
                GetPokeDataResponse getPokeDataResponse = response.getBody().as(GetPokeDataResponse.class);
                pokeapiPokemon.pokemonNumber = getPokeDataResponse.getId();

                for ( int i=0; i < getPokeDataResponse.getTypes().size(); i++) {
                    pokeapiPokemon.pokemonTypeList.add(getPokeDataResponse.getTypes().get(i).getType().getName().toLowerCase());
                }

                for( int i=0; i < getPokeDataResponse.getStats().size(); i++) {
                    pokeapiPokemon.pokemonBaseStats.put(PokemonUtil.baseStatsKey.get(i),
                            getPokeDataResponse.getStats().get(i).getBase_stat());
                }
                writePokemonToCSV(pokeapiPokemon, getProperty("path.csv.pokeapi"));
            }
            else {
                writePokemonNotFoundMessageToCSV( pokemonName, getProperty("path.csv.pokeapi"));
            }
        }
    }

    @Then("get all pokemon data from pokeapi should be success")
    public void getAllPokemonDataFromPokeapiShouldBeSuccess() {
        softAssert.assertAll();
    }
}
