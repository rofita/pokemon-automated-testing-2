package com.blibli.future.rofita.runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/features/",
        stepNotifications = true,
        glue="com.blibli.future.rofita",
        tags = "@GetPokemondbDataThenCompare"
)
public class PokemondbRunner {}
