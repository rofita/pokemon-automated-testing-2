package com.blibli.future.rofita;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/features/",
        stepNotifications = true,
        glue="com.blibli.future.rofita",
        tags = "@ComparePokemonDataDirectly"
)
public class CucumberRunner {}
