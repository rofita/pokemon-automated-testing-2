@GetPokeapiDataThenCompare @GetPokemonDataThenCompare

Feature: Get Pokemon Data from PokeAPI

  @GetPokeapiData @GetPokemonData
  Scenario: Get All Pokemon Data from PokeApi
    Given prepare to get pokemon data from pokeapi
    When  get all data from pokeapi
    And   ensure from 'pokeapi' runner that pokemon data from other sources are ready
    Then  get all pokemon data from pokeapi should be success

  Scenario Outline: Compare '<pokemon>' Data from All Sources if Data is Ready
    Given check that pokemon data is ready to compare using 'pokeapi' runner
    And   prepare to compare '<pokemon>' data from all sources
    When  compare pokemon data from 'bulbapedia' and 'pokemondb'
    And   compare pokemon data from 'bulbapedia' and 'pokedex'
    And   compare pokemon data from 'bulbapedia' and 'pokeapi'
    And   compare pokemon data from 'pokemondb' and 'pokedex'
    And   compare pokemon data from 'pokemondb' and 'pokeapi'
    And   compare pokemon data from 'pokedex' and 'pokeapi'
    Then  pokemon data from all sources is valid

    Examples:
      | pokemon   |
      | Pikachu   |
      | Charizard |
      | Eevee     |
      | Mewtwo    |
      | Garurumon |
      | Koffing   |
      | Meowth    |
      | Blastoise |
      | Ivysaur   |
      | Squirtle  |
      | Mew       |
      | Pidgey    |
      | Rattata   |