@GetPokedexDataThenCompare @GetPokemonDataThenCompare

Feature: Get Pokemon Data from Pokedex

  @GetPokedexData @GetPokemonData
  Scenario: Get All Pokemon Data from Pokedex
    Given set up android driver
    And   prepare to get pokemon data from pokedex
    And   get all pokemon data from pokedex
    And   ensure from 'pokedex' runner that pokemon data from other sources are ready
    Then  get all pokemon data from pokedex should be success

  Scenario Outline: Compare '<pokemon>' Data from All Sources if Data is Ready
    Given check that pokemon data is ready to compare using 'pokedex' runner
    And   prepare to compare '<pokemon>' data from all sources
    When  compare pokemon data from 'bulbapedia' and 'pokemondb'
    And   compare pokemon data from 'bulbapedia' and 'pokedex'
    And   compare pokemon data from 'bulbapedia' and 'pokeapi'
    And   compare pokemon data from 'pokemondb' and 'pokedex'
    And   compare pokemon data from 'pokemondb' and 'pokeapi'
    And   compare pokemon data from 'pokedex' and 'pokeapi'
    Then  pokemon data from all sources is valid

    Examples:
      | pokemon   |
      | Pikachu   |
      | Charizard |
      | Eevee     |
      | Mewtwo    |
      | Garurumon |
      | Koffing   |
      | Meowth    |
      | Blastoise |
      | Ivysaur   |
      | Squirtle  |
      | Mew       |
      | Pidgey    |
      | Rattata   |