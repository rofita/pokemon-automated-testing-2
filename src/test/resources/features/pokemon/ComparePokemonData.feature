Feature: Compare Pokemon Data

  @ComparePokemonDataDirectly
  Scenario Outline: Compare '<pokemon>' Data from All Sources
    Given prepare to compare '<pokemon>' data from all sources
    When  compare pokemon data from 'bulbapedia' and 'pokemondb'
    And   compare pokemon data from 'bulbapedia' and 'pokedex'
    And   compare pokemon data from 'bulbapedia' and 'pokeapi'
    And   compare pokemon data from 'pokemondb' and 'pokedex'
    And   compare pokemon data from 'pokemondb' and 'pokeapi'
    And   compare pokemon data from 'pokedex' and 'pokeapi'
    Then  pokemon data from all sources is valid

    Examples:
      | pokemon   |
      | Pikachu   |
      | Charizard |
      | Eevee     |
      | Mewtwo    |
      | Garurumon |
      | Koffing   |
      | Meowth    |
      | Blastoise |
      | Ivysaur   |
      | Squirtle  |
      | Mew       |
      | Pidgey    |
      | Rattata   |
