@GetPokemondbDataThenCompare @GetPokemonDataThenCompare

Feature: Get Pokemon Data from PokemonDB

  @GetPokemondbData @GetPokemonData
  Scenario: Get All Pokemon Data from PokemonDB
    Given set up web driver
    And   prepare to get pokemon data from pokemondb
    And   open pokemon list page in pokemondb
    When  get all pokemon data from pokemondb
    And   ensure from 'pokemondb' runner that pokemon data from other sources are ready
    Then  get all pokemon data from pokemondb should be success

  Scenario Outline: Compare '<pokemon>' Data from All Sources if Data is Ready
    Given check that pokemon data is ready to compare using 'pokemondb' runner
    And   prepare to compare '<pokemon>' data from all sources
    When  compare pokemon data from 'bulbapedia' and 'pokemondb'
    And   compare pokemon data from 'bulbapedia' and 'pokedex'
    And   compare pokemon data from 'bulbapedia' and 'pokeapi'
    And   compare pokemon data from 'pokemondb' and 'pokedex'
    And   compare pokemon data from 'pokemondb' and 'pokeapi'
    And   compare pokemon data from 'pokedex' and 'pokeapi'
    Then  pokemon data from all sources is valid

    Examples:
      | pokemon   |
      | Pikachu   |
      | Charizard |
      | Eevee     |
      | Mewtwo    |
      | Garurumon |
      | Koffing   |
      | Meowth    |
      | Blastoise |
      | Ivysaur   |
      | Squirtle  |
      | Mew       |
      | Pidgey    |
      | Rattata   |