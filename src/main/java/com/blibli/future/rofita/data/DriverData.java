package com.blibli.future.rofita.data;

import static com.blibli.future.rofita.utils.PokemonUtil.getProperty;

public class DriverData {
    private static String platformName;

    public static String getPlatformName(){
        return platformName;
    }

    public static void setPlatformName(String value){
        platformName = value;
    }

    public static Boolean isRemote(){
        return getProperty("browser.remote").equalsIgnoreCase("y");
    }
}
