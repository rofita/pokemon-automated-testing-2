package com.blibli.future.rofita.data;
import java.util.HashMap;
import java.util.List;

public class Pokemon {
    public String pokemonName;
    public int pokemonNumber;
    public List<String> pokemonTypeList;
    public HashMap<String, Integer> pokemonBaseStats;
}
