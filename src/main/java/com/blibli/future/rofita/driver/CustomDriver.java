package com.blibli.future.rofita.driver;

import com.blibli.future.rofita.data.DriverData;
import com.blibli.future.rofita.utils.UIUtil;
import io.appium.java_client.android.AndroidDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class CustomDriver implements DriverSource {

    @Override
    public WebDriver newDriver() {

        WebDriver webDriver;
        AndroidDriver androidDriver;

        String customDriverName = DriverData.getPlatformName();
        Boolean isRemote = DriverData.isRemote();

        if (customDriverName.equalsIgnoreCase("android")) {
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setCapability("platformName", "Android");
            desiredCapabilities.setCapability("automationName", "UiAutomator1");
            desiredCapabilities.setCapability("udid", "YDPZLJROEQ4T955T");
            desiredCapabilities.setCapability("appPackage", "dev.ronnie.pokeapiandroidtask");
            desiredCapabilities.setCapability("appActivity", "dev.ronnie.pokeapiandroidtask.MainActivity");
            desiredCapabilities.setCapability("unicodeKeyboard", "true");
            desiredCapabilities.setCapability("resetKeyboard", "true");

            URL url = null;
            try {
                url = new URL("http://127.0.0.1:4723/wd/hub");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            if(url!=null){
                androidDriver = new AndroidDriver<>(url,desiredCapabilities);
                UIUtil.ANDROID_DRIVER = androidDriver;
                return androidDriver;
            }
            else{
                return null;
            }
        }
        else if (!isRemote && customDriverName.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            System.out.println("chrome driver is started");
            webDriver = new ChromeDriver();
            return webDriver;
        }
        else if (!isRemote && customDriverName.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            System.out.println("firefox driver is started");
            webDriver = new FirefoxDriver();
            return webDriver;
        }
        else if (isRemote && customDriverName.equalsIgnoreCase("chrome")) {
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setCapability("name", "chromeTest");
            desiredCapabilities.setCapability("browserName", "chrome");
            desiredCapabilities.setCapability("enableVNC", true);
            desiredCapabilities.setCapability("enableVideo", true);
            desiredCapabilities.setCapability("enableLog", true);

            URL url = null;
            try {
                url = new URL("http://localhost:4444/wd/hub");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            if(url!=null){
                return new RemoteWebDriver(url,desiredCapabilities);
            }
            else{
                return null;
            }
        }
        else if (isRemote && customDriverName.equalsIgnoreCase("firefox")) {
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setCapability("name", "firefoxTest");
            desiredCapabilities.setCapability("browserName", "firefox");
            desiredCapabilities.setCapability("enableVNC", true);
            desiredCapabilities.setCapability("enableVideo", true);
            desiredCapabilities.setCapability("enableLog", true);

            URL url = null;
            try {
                url = new URL("http://localhost:4444/wd/hub");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            if(url!=null){
                return new RemoteWebDriver(url,desiredCapabilities);
            }
            else{
                return null;
            }
        }
        else {
            throw new UnsupportedOperationException("driver is not supported");
        }
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
