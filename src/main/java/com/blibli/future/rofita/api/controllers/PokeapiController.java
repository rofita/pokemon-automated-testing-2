package com.blibli.future.rofita.api.controllers;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class PokeapiController {

    public Response getPokemonData(String pokemonName) {

        Response response = SerenityRest.given()
                .header("Content-Type", "application/json")
                .when()
                .get("https://pokeapi.co/api/v2/pokemon/" + pokemonName);

        return response;

    }
}
