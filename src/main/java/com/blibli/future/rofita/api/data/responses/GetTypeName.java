package com.blibli.future.rofita.api.data.responses;

import lombok.Data;

@Data
public class GetTypeName {
    private String name;
    private String url;
}
