package com.blibli.future.rofita.api.data.responses;

import lombok.Data;

@Data
public class GetStatName {
    private String name;
    private String url;
}
