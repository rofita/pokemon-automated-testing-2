package com.blibli.future.rofita.api.data.responses;

import lombok.Data;

@Data
public class GetTypeData {
    private int slot;
    private GetTypeName type;
}
