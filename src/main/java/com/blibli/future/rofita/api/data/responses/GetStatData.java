package com.blibli.future.rofita.api.data.responses;

import lombok.Data;

@Data
public class GetStatData {
    private int base_stat;
    private int effort;
    private GetStatName stat;
}
