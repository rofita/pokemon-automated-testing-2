package com.blibli.future.rofita.utils;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.blibli.future.rofita.data.Pokemon;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.valueOf;

public class PokemonUtil {
    public static final List<String> baseStatsKey = Arrays.asList
                    ("HP","Attack","Defense","Special Attack","Special Defense","Speed");

    public static String getProperty(String key) {
        EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
        return environmentVariables.getProperty(key);
    }

    public static void clearCsvFile(String path) throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter(path));
        csvWriter.flush();
        csvWriter.close();
    }

    public static String getPathFromDataSource(String dataSource) {
        dataSource = dataSource.trim().toLowerCase();

        switch(dataSource) {
            case "bulbapedia":
                return getProperty("path.csv.bulbapedia");
            case "pokemondb":
                return getProperty("path.csv.pokemondb");
            case "pokeapi":
                return getProperty("path.csv.pokeapi");
            case "pokedex":
                return getProperty("path.csv.pokedex");
            default:
                return "data source is not valid";
        }
    }

    public static Pokemon readPokemonFromCSV(String pokeName, String path) throws IOException {
        Pokemon pokemon = new Pokemon();
        CSVReader reader = new CSVReader(new FileReader(path));
        String [] nextLine;
        int columnIndexOfPokeName = 0;
        while ((nextLine = reader.readNext()) != null) {
            if(nextLine[columnIndexOfPokeName].matches(pokeName)){
                pokemon.pokemonName = nextLine[0];
                pokemon.pokemonNumber = Integer.parseInt(nextLine[1]);
                pokemon.pokemonTypeList = Arrays.asList(nextLine[2].split(" "));
                pokemon.pokemonBaseStats = new HashMap<>();
                List<String> baseStatsArr = Arrays.asList((nextLine[3].split(" ")));
                for( int i=0 ; i < baseStatsArr.size() ; i++) {
                    pokemon.pokemonBaseStats.put(PokemonUtil.baseStatsKey.get(i),
                            (valueOf(baseStatsArr.get(i))));
                }
            }
        }
        reader.close();
        return pokemon;
    }

    public static boolean isPokemonDataInCsvFilesReady(int totalPokemon) throws IOException {
        return countCsvLines(getProperty("path.csv.pokedex")) == totalPokemon
                && countCsvLines(getProperty("path.csv.pokeapi")) == totalPokemon
                && countCsvLines(getProperty("path.csv.pokemondb")) == totalPokemon
                && countCsvLines(getProperty("path.csv.bulbapedia")) == totalPokemon;
    }

    public static boolean isThisLastRunner(int totalRunner) throws IOException {
        return countCsvLines("target/runnernotready.csv") >= totalRunner-1;
    }

    public static int countCsvLines(String path) throws IOException {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            return 0;
        }

        String [] nextLine;
        int lineCount = 0;

        while ((nextLine = reader.readNext()) != null) {
            if(!nextLine[0].trim().equalsIgnoreCase(""))
                lineCount++;
        }

        reader.close();
        return lineCount;
    }

    public static void writeSignNotReadyToCSV(String source) throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter("target/runnernotready.csv", true));
        String [] nextLine = new String[1];

        nextLine[0] = source;

        csvWriter.writeNext(nextLine);
        csvWriter.close();
    }

    public static boolean isRunnerReadyDoCompare(String runner) throws IOException {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("target/runnernotready.csv"));
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
            return false;
        }

        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            if(nextLine[0].trim().equalsIgnoreCase(runner)) {
                reader.close();
                return false;
            }
        }

        reader.close();
        return true;
    }

    public static void writePokemonNotFoundMessageToCSV (String pokemonName, String path) throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter(path, true));
        String [] nextLine = new String[1];

        nextLine[0] = String.format("%s data is not found", pokemonName);

        csvWriter.writeNext(nextLine);
        csvWriter.close();
    }

    public static void writePokemonToCSV(Pokemon pokemon, String path) throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter(path, true));

        String [] nextLine = new String[4];
        nextLine[0] = pokemon.pokemonName;
        nextLine[1] = String.valueOf(pokemon.pokemonNumber);

        if (pokemon.pokemonTypeList.size() > 0){
            nextLine[2] = pokemon.pokemonTypeList.get(0) ;
            for ( int i=1; i < pokemon.pokemonTypeList.size(); i++) {
                nextLine[2] += " " + pokemon.pokemonTypeList.get(i);
            }
        }

        if (pokemon.pokemonBaseStats.size() > 0){
            nextLine[3] = "";
            for (String baseStat : PokemonUtil.baseStatsKey) {
                nextLine[3] += pokemon.pokemonBaseStats.get(baseStat) + " ";
            }
        }

        csvWriter.writeNext(nextLine);
        csvWriter.close();
    }

    public static List<String> getPokemonNameList() {
        List<String> pokemonNameList = Arrays.asList(getProperty("pokemon.name").split(","));

        for (int i = 0; i < pokemonNameList.size(); i++) {
            String str = pokemonNameList.get(i).toLowerCase();
            pokemonNameList.set( i, str.substring(0,1).toUpperCase() + str.substring(1));
        }
        return pokemonNameList;
    }
}
