package com.blibli.future.rofita.utils;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class UIUtil extends PageObject {

    public static AndroidDriver ANDROID_DRIVER;

    protected void hoverElement(WebElementFacade webElementFacade) {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(webElementFacade).build().perform();
    }

    protected void clickElementUsingJSE (WebElementFacade webElementFacade) {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].click();", webElementFacade);
    }

    protected void clickElementByXpath(String xpath) {
        By element = By.xpath(xpath);
        waitFor(ExpectedConditions.presenceOfElementLocated(element));
        find(element).click();
    }

    protected boolean isVisibleByXpath (String xpath) {
        return find(By.xpath(xpath)).isVisible();
    }

    protected List<WebElementFacade> getElementsByXpath (String xpath) {
        return findAll(By.xpath(xpath));
    }

    protected WebElement getElementByXpath (String xpath) {
        return getDriver().findElement(By.xpath(xpath));
    }

    protected List <String> getStringListFromElements(List<WebElementFacade> webElementFacades) {
        List<String> stringList = new ArrayList<>();
        for (WebElementFacade w : webElementFacades) {
            stringList.add(w.getText());
        }
        return stringList;
    }

    protected static void swipeScreenAndroid(int startX,int startY,int endX,int endY, int duration){
        new TouchAction((PerformsTouchActions) ANDROID_DRIVER)
                .press(new PointOption().withCoordinates(startX, startY))
                .waitAction(new WaitOptions().withDuration(Duration.ofMillis(duration)))
                .moveTo(new PointOption().withCoordinates(endX, endY)).release().perform();
    }

    protected void pressEnterAndroid(){
        ANDROID_DRIVER.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

}
