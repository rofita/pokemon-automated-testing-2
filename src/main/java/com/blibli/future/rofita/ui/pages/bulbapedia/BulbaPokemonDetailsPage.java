package com.blibli.future.rofita.ui.pages.bulbapedia;

import com.blibli.future.rofita.utils.PokemonUtil;
import com.blibli.future.rofita.utils.UIUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class BulbaPokemonDetailsPage extends UIUtil {

    @FindBy(xpath = "//div[@id='mw-content-text']//table[2]//a[contains(@title,'Pokédex number')]/span")
    WebElementFacade pokemonNumberField;

    @FindBy(xpath = "//span[@id='Base_stats']/parent::*/following-sibling::table[1]//a[@title='Stat' or 'HP']/parent::div/following-sibling::*[1]")
    List<WebElementFacade> singleGenerationPokemonBaseStatsField;

    @FindBy(xpath = "//span[@id='Base_stats']/parent::*/following-sibling::h5/following-sibling::*//a[@title='Stat']//ancestor::table//preceding-sibling::h5//span[contains(@id,'Generation')]")
    List<WebElementFacade> multiGenerationPokemonBaseStatsTableHeadline;

    @FindBy(xpath = "//a[@title='Stat']//ancestor::table//preceding-sibling::h5//span[contains(@id,'Generation')]/parent::*/following-sibling::table[1]//a[@title='Stat' or 'HP']/parent::div/following-sibling::div[1]")
    List<WebElementFacade> multiGenerationPokemonBaseStatsField;

    @FindBy(xpath = "//div[@id='mw-content-text']//table[2]//a[@title='Type']/parent::*/following-sibling::*//td[1]/table//b[not(text()='Unknown')]")
    List<WebElementFacade> pokemonTypesField;

    private String pokemonNameFieldXpathFormat = "//div[@id='mw-content-text']//table[2]//b[text()='%s']";

    public boolean isTherePokemon(String pokemonName) {
        return isVisibleByXpath(String.format(pokemonNameFieldXpathFormat,pokemonName));
    }

    public HashMap<String,Integer> getPokemonBaseStats() {
        int totalBaseStatsTableOfPokemonGeneration;
        int totalBaseStatsFields;
        int pokemonBaseStatsFirstSeq;
        List<WebElementFacade> pokemonBaseStatsElements;

        if (multiGenerationPokemonBaseStatsTableHeadline.size()>0) {
            pokemonBaseStatsElements = multiGenerationPokemonBaseStatsField;
            totalBaseStatsTableOfPokemonGeneration = multiGenerationPokemonBaseStatsTableHeadline.size();
            totalBaseStatsFields = multiGenerationPokemonBaseStatsField.size();
            pokemonBaseStatsFirstSeq = totalBaseStatsFields - (totalBaseStatsFields / totalBaseStatsTableOfPokemonGeneration);
        }
        else {
            pokemonBaseStatsElements = singleGenerationPokemonBaseStatsField;
            pokemonBaseStatsFirstSeq = 0;
        }

        HashMap<String, Integer> pokemonBaseStatsHashMap = new HashMap<>();

        for(int i = 0; i < PokemonUtil.baseStatsKey.size() ; i++) {
            pokemonBaseStatsHashMap.put(PokemonUtil.baseStatsKey.get(i),
                    (valueOf(pokemonBaseStatsElements.get(pokemonBaseStatsFirstSeq+i).getText())));
        }
        return pokemonBaseStatsHashMap;
    }

    public int getPokemonNumber() {
        int pokemonNumberInt = parseInt(pokemonNumberField.getText().replace('#', '0'));
        return pokemonNumberInt;
    }

    public List<String> getPokemonTypes() {
        List<String> pokemonTypeList = new ArrayList<>();

        for(WebElement element : pokemonTypesField) {
            pokemonTypeList.add(element.getText().toLowerCase());
        }
        return pokemonTypeList;
    }

}