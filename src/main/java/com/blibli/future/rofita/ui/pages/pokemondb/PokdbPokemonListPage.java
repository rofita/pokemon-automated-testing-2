package com.blibli.future.rofita.ui.pages.pokemondb;

import com.blibli.future.rofita.utils.PokemonUtil;
import com.blibli.future.rofita.utils.UIUtil;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class PokdbPokemonListPage extends UIUtil {

    private String pokemonNameFieldXpath;
    private String pokemonNumberFieldXpath;
    private String pokemonTypesFieldXpath;
    private String pokemonBaseStatsFieldXpath;

    private String pokemonNameFieldXpathFormat = "//table[@id='pokedex']/descendant::a[@class='ent-name' and text()='%s'][1]";
    private String pokemonNumberFieldXpathFormat = pokemonNameFieldXpathFormat + "/parent::*/preceding-sibling::*/child::span[@class='infocard-cell-data']";
    private String pokemonBaseStatsFieldXpathFormat = pokemonNameFieldXpathFormat + "/parent::*/following-sibling::td[@class='cell-num']";
    private String pokemonTypesFieldXpathFormat = pokemonNameFieldXpathFormat + "/parent::*/following-sibling::td[@class='cell-icon']/child::a";

    public boolean isTherePokemon(String pokemonName) {
        return isVisibleByXpath(String.format(pokemonNameFieldXpathFormat,pokemonName));
    }

    public void setPokemonName(String pokemonName) {
        pokemonNameFieldXpath = String.format(pokemonNameFieldXpathFormat, pokemonName);
        pokemonNumberFieldXpath = String.format(pokemonNumberFieldXpathFormat, pokemonName);
        pokemonBaseStatsFieldXpath = String.format(pokemonBaseStatsFieldXpathFormat, pokemonName);
        pokemonTypesFieldXpath = String.format(pokemonTypesFieldXpathFormat, pokemonName);
    }

    public int getPokemonNumber() {
        int pokemonNumberInt = parseInt(getElementByXpath(pokemonNumberFieldXpath).getText());
        return pokemonNumberInt;
    }

    public HashMap<String,Integer> getPokemonBaseStats() {
        List<WebElementFacade> pokemonBaseStatsElements = getElementsByXpath(pokemonBaseStatsFieldXpath);
        HashMap<String, Integer> pokemonBaseStatsString = new HashMap<>();

        for( int i=0 ; i < pokemonBaseStatsElements.size() ; i++) {
            pokemonBaseStatsString.put(PokemonUtil.baseStatsKey.get(i),
                    (valueOf(pokemonBaseStatsElements.get(i).getText())));
        }

        return pokemonBaseStatsString;
    }

    public List<String> getPokemonTypes() {
        List<WebElementFacade> pokemonTypeElements = getElementsByXpath(pokemonTypesFieldXpath);
        List<String> pokemonTypeList = new ArrayList<>();

        for(WebElement element : pokemonTypeElements) {
            pokemonTypeList.add(element.getText().toLowerCase());
        }

        return pokemonTypeList;
    }
}

