package com.blibli.future.rofita.ui.pages.bulbapedia;

import com.blibli.future.rofita.utils.UIUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.Keys;

@DefaultUrl("https://bulbapedia.bulbagarden.net/")
public class BulbaHomePage extends UIUtil {

    @FindBy(xpath = "//input[@id='searchInput']")
    WebElementFacade searchBar;

    public void searchPokemonUsingSearchBar(String pokemonName) {
        searchBar.sendKeys(pokemonName);
        searchBar.sendKeys(Keys.ENTER);
    }

}
