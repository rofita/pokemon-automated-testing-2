package com.blibli.future.rofita.ui.pages.pokedex;

import com.blibli.future.rofita.utils.UIUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class PokedexHomePage extends UIUtil {

    @FindBy(id = "confirm_button")
    WebElementFacade popupConfirmButton;

    @FindBy(id = "searchView")
    WebElementFacade searchBar;

    @FindBy(id = "pokemon_item_title")
    List<WebElementFacade> pokemonNameText;

    private int pokemonCardIndex;

    public void closePopup() {
        if (popupConfirmButton.isVisible())
            popupConfirmButton.click();
    }

    public void searchPokemonUsingSearchBar(String pokemonName) {
        searchBar.clear();
        searchBar.sendKeys(pokemonName);
        pressEnterAndroid();
        pressEnterAndroid();
    }

    public boolean isTherePokemon(String pokemonName) {
        if (pokemonNameText.size()<=0) return false;
        for (int i=0 ; i<pokemonNameText.size(); i++){
            if (pokemonNameText.get(i).getText().equalsIgnoreCase(pokemonName)){
                pokemonCardIndex = i;
                return true;
            }
        }
        return false;
    }

    public void openPokemonCard() {
        pokemonNameText.get(pokemonCardIndex).waitUntilClickable();
        pokemonNameText.get(pokemonCardIndex).click();
    }

}
