package com.blibli.future.rofita.ui.pages.pokemondb;

import com.blibli.future.rofita.utils.UIUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://pokemondb.net/")
public class PokdbHomePage extends UIUtil {

    @FindBy(xpath = "//main[@id='main']/preceding-sibling::*[1]/descendant::*[text()='Pokémon data']")
    WebElementFacade pokemonDataTab;

    @FindBy(xpath = "//ul[@class='main-menu-sub']/li/a[@href='/pokedex/all']")
    WebElementFacade pokemonListMenu;

    @FindBy(xpath = "//button[@class='btn btn-primary gdpr-accept']")
    WebElementFacade popupOkButton;

    public void closePopup() {
        if (popupOkButton.isVisible()){
            popupOkButton.click();
        }
    }

    public void goToPokemonListPage() throws InterruptedException {
        Thread.sleep(3000);
        hoverElement(pokemonDataTab);
        Thread.sleep(3000);
        pokemonListMenu.click();
    }

}