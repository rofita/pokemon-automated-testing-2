package com.blibli.future.rofita.ui.pages.pokedex;

import com.blibli.future.rofita.utils.PokemonUtil;
import com.blibli.future.rofita.utils.UIUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.valueOf;

public class PokedexPokemonDetailsPage extends UIUtil {

    @FindBy(id = "pokemon_number")
    WebElementFacade pokemonNumberField;

    @FindBy(id = "stat_count")
    List<WebElementFacade> pokemonBaseStatsElements;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    WebElementFacade backButton;

    public HashMap<String,Integer> getPokemonBaseStats() {
        waitFor(pokemonBaseStatsElements.get(0));
        swipeScreenAndroid(350,1200,350,850, 1000);

        HashMap pokemonBaseStatsHashMap = new HashMap<>();
        for(int i = 0; i < PokemonUtil.baseStatsKey.size() ; i++) {
            pokemonBaseStatsHashMap.put(PokemonUtil.baseStatsKey.get(i),
                    (valueOf(pokemonBaseStatsElements.get(i).getText().replace(".0",""))));
        }
        return pokemonBaseStatsHashMap;
    }

    public int getPokemonNumber() {
        waitFor(pokemonNumberField);

        int pokemonNumberInt = parseInt(pokemonNumberField.getText());
        return pokemonNumberInt;
    }

    public void backToHomePage() {
        backButton.click();
    }

}